﻿#include <iostream>
#include<array>

class Animal
{
public:
	virtual void Voice() = 0;
};

class Dog :public Animal
{
public:
	void Voice() override { std::cout << "Woof! " << std::endl; };
};

class Cat :public Animal
{
public:
	void Voice() override { std::cout << "Murr " << std::endl; };
};

class Bird :public Animal
{
public:
	void Voice() override { std::cout << "Chik-chirik " << std::endl; };
};

int main()
{
	Animal* arr[] = { new Dog, new Cat, new Bird };
	for (Animal* a : arr) a->Voice();
}
